<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

class kirimEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:kirim';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'kirim Email';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $data = DB::table('recipient')->where([
            "sent" => 0
        ])->get();
        foreach ($data as $list) {
            $mail = new PHPMailer(true);
            try {
                $from = env("SMTP_USERNAME");
                // $mail->SMTPDebug = SMTP::DEBUG_SERVER;
                $mail->isSMTP();
                $mail->Host       = 'smtp.gmail.com';
                $mail->SMTPAuth   = true;
                $mail->Username   = env("SMTP_USERNAME");
                $mail->Password   = env("SMTP_PASSWORD");
                $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
                $mail->Port       = 587;
                $mail->setFrom($from, $from);
                $mail->addAddress($list->email);
                $mail->isHTML(true);
                $mail->Subject = env("MAIL_SUBJECT");
                $mail->Body    = file_get_contents(public_path() . "/email.txt");
                $mail->send();
                echo $list->email . " berhasil terkirim \n";
                sleep(10);
            } catch (\Throwable $th) {
                echo $list->email . " Gagal terkirim \n";
                //throw $th;
            }
            DB::table('recipient')->where([
                "id" => $list->id
            ])->update([
                "sent" => 1
            ]);
        }
    }
}
